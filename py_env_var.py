# Python program to explain os.environ object  
# importing os module  
import numpy as np
import cv2
import matplotlib
from matplotlib import colors
from matplotlib import pyplot as plt
import os

import pymysql

# Open database connection
db = pymysql.connect(os.environ['host_name'],os.environ['host_user'],os.environ['password'],os.environ['database_name'])

# prepare a cursor object using cursor() method
cursor = db.cursor()

images = 'o_wbc'
o_path = os.environ['data_path']+'/org_wbc'
b_path = os.environ['data_path']+'/b_wbc'
c_path = os.environ['data_path']+'/c_wbc'

j = 0
for image in os.listdir(images):
    im = cv2.imread(images+'/'+image)
    j = j + 1
    im1 = im.copy()
    hsv_img = cv2.cvtColor(im, cv2.COLOR_BGR2HSV)

    COLOR_MIN = np.array([125, 70, 20],np.uint8)
    COLOR_MAX = np.array([170, 255, 255],np.uint8)
    frame_threshed = cv2.inRange(hsv_img, COLOR_MIN, COLOR_MAX)

    imgray = frame_threshed
    ret,thresh = cv2.threshold(frame_threshed,127,255,0)

    kernel = np.ones((5,5), np.uint8) 
    thresh = cv2.erode(thresh, kernel, iterations=1)
    contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
    contours1 = sorted(contours, key = cv2.contourArea, reverse = True)

    areas = [cv2.contourArea(c) for c in contours1[0:20]]
    thr = np.mean(areas)
    i = 0
    for h,cnt in enumerate(contours1[:]):
        if cv2.contourArea(cnt)<=thr:
            break
        x,y,w,h = cv2.boundingRect(cnt)
        x1=x+w//2-150
        y1=y+h//2-150
        x2=x+w//2+150
        y2=y+h//2+150
        # print(x,y,w,h)
        if(x1>=0 and y1>=0 and x2<=3263 and y2<=2447):
            roi = im1[y1:y2,x1:x2]
            i = i + 1
            wbc_path = 'c_wbc/'+'wbc_'+str(j)+'_'+str(i)+'.jpg'
            img_path = 'org_wbc/wbc_'+str(j)+'.jpg'
            query = "INSERT INTO test_img(wbc_path, x0, y0, x1, y1, img_path) VALUES(%s,%s,%s,%s,%s,%s);"
            args = (wbc_path, x1, y1, x2, y2, img_path)
            # execute SQL query using execute() method.
            cursor.execute(query,args)
            db.commit()
            #cv2.imwrite(os.path.join(c_path, 'wbc_'+str(j)+'_'+str(i)+'.jpg'),roi)
            # show(roi)
        #cv2.rectangle(im,(x1,y1),(x2,y2),(0,255,0),2)
    # show(im)
    #cv2.imwrite(os.path.join(b_path , 'b_wbc_'+str(j)+'.jpg'), im)
    #cv2.imwrite(os.path.join(o_path , 'wbc_'+str(j)+'.jpg'), im1)